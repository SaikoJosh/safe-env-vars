import * as main from './main';
import EnvironmentReader from './lib/EnvironmentReader';

describe(`exports`, () => {
	test(`EnvironmentReader is exported`, () => {
		expect(main.EnvironmentReader).toBeDefined();
		expect(main.EnvironmentReader).toBe(EnvironmentReader);
	});

	test(`EnvironmentLoader is exported`, () => {
		expect(main.EnvironmentLoader).toBeDefined();
		expect(main.EnvironmentLoader).toBe(EnvironmentReader);
	});
});
