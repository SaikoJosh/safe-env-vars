import EnvironmentReader from './lib/EnvironmentReader';
import type { ClassOptionsInput, GetOptionsInput } from './lib/EnvironmentReader';
import type { DataType } from './lib/definitions';

export const EnvironmentLoader = EnvironmentReader; // Export both names.
export { EnvironmentReader }; // Export both names.
export type { ClassOptionsInput, GetOptionsInput, DataType };
