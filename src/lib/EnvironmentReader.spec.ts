import path from 'path';
import EnvironmentReader from './EnvironmentReader';

let defaultProcessEnv: NodeJS.ProcessEnv;

beforeEach(() => {
	defaultProcessEnv = { ...process.env };
});

afterEach(() => {
	process.env = { ...defaultProcessEnv };
});

describe(`constructor`, () => {
	test(`class can be instantiated`, () => {
		const env = new EnvironmentReader();

		expect(env instanceof EnvironmentReader).toBe(true);
	});

	test(`dot env file is loaded by default`, () => {
		const dotEnvPath = path.resolve(__dirname, `..`, `..`, `test`, `test.env`);
		const env = new EnvironmentReader({ dotEnv: true, dotEnvPath });

		const result = env.get(`LOADED_FROM_DOT_ENV`);

		expect(result).toBe(`yep!`);
	});

	test(`dot env functionality can be disabled`, () => {
		const dotEnvPath = path.resolve(__dirname, `..`, `..`, `test`, `test.env`);
		const env = new EnvironmentReader({ dotEnv: false, dotEnvPath });

		const result = env.optional.get(`LOADED_FROM_DOT_ENV`);

		expect(result).toBe(undefined);
	});
});

describe(`methods`, () => {
	describe(`general read logic`, () => {
		test(`throws an error for an invalid key`, () => {
			const env = new EnvironmentReader();

			expect(() => env.get(`this is an invalid key`)).toThrow(/key/i);
		});

		test(`throws an error for an empty string by default`, () => {
			process.env.SOME_ENV = ``;
			const env = new EnvironmentReader();

			expect(() => env.get(`SOME_ENV`)).toThrow(/empty/i);
		});

		test(`does not throw an error for an empty string if explicitly allowed`, () => {
			process.env.SOME_ENV = ``;
			const env = new EnvironmentReader();

			const result = env.get(`SOME_ENV`, { allowEmpty: true });

			expect(typeof result).toBe(`string`);
			expect(result).toBe(``);
		});

		describe(`allowed values`, () => {
			describe(`strings`, () => {
				test(`throws an error if the value is not in the allow list`, () => {
					process.env.SOME_ENV = `some-bad-value`;
					const allowedValues = [`ABC`, `DEF`];
					const env = new EnvironmentReader();

					expect(() => env.string.get(`SOME_ENV`, { allowedValues })).toThrow(/one of/i);
				});

				test(`does not throw an error when allowed values is set and the value is optional`, () => {
					process.env.SOME_ENV = undefined;
					const allowedValues = [`ABC`, `DEF`];
					const env = new EnvironmentReader();

					expect(() => env.optional.string.get(`SOME_ENV`, { allowedValues })).not.toThrow();
				});
			});

			describe(`numbers`, () => {
				test(`throws an error if the value is not in the allow list`, () => {
					process.env.SOME_ENV = `666`;
					const allowedValues = [111, 222];
					const env = new EnvironmentReader();

					expect(() => env.number.get(`SOME_ENV`, { allowedValues })).toThrow(/one of/i);
				});

				test(`does not throw an error when allowed values is set and the value is optional`, () => {
					process.env.SOME_ENV = undefined;
					const allowedValues = [111, 222];
					const env = new EnvironmentReader();

					expect(() => env.optional.number.get(`SOME_ENV`, { allowedValues })).not.toThrow();
				});
			});

			describe(`booleans`, () => {
				test(`throws an error if the value is not in the allow list`, () => {
					process.env.SOME_ENV = `false`;
					const allowedValues = [true];
					const env = new EnvironmentReader();

					expect(() => env.boolean.get(`SOME_ENV`, { allowedValues })).toThrow(/one of/i);
				});

				test(`does not throw an error when allowed values is set and the value is optional`, () => {
					process.env.SOME_ENV = undefined;
					const allowedValues = [true];
					const env = new EnvironmentReader();

					expect(() => env.optional.boolean.get(`SOME_ENV`, { allowedValues })).not.toThrow();
				});
			});
		});
	});

	describe(`env.get()`, () => {
		test(`returns a string if the env var exists`, () => {
			process.env.SOME_ENV = `some-string`;
			const env = new EnvironmentReader();

			const result = env.get(`SOME_ENV`);

			expect(typeof result).toBe(`string`);
			expect(result).toBe(`some-string`);
		});

		test(`throws an error if the env var doesn't exist`, () => {
			process.env.SOME_ENV = undefined;
			const env = new EnvironmentReader();

			expect(() => env.get(`SOME_ENV`)).toThrow(/not set/i);
		});
	});

	describe(`env.optional.get()`, () => {
		test(`returns a string if the env var exists`, () => {
			process.env.SOME_ENV = `some-string`;
			const env = new EnvironmentReader();

			const result = env.optional.get(`SOME_ENV`);

			expect(typeof result).toBe(`string`);
			expect(result).toBe(`some-string`);
		});

		test(`returns undefined if the env var doesn't exist`, () => {
			process.env.SOME_ENV = undefined;
			const env = new EnvironmentReader();

			const result = env.optional.get(`SOME_ENV`);

			expect(result).toBe(undefined);
		});
	});

	describe(`env.string.get()`, () => {
		test(`returns a string if the env var exists`, () => {
			process.env.SOME_ENV = `some-string`;
			const env = new EnvironmentReader();

			const result = env.string.get(`SOME_ENV`);

			expect(typeof result).toBe(`string`);
			expect(result).toBe(`some-string`);
		});

		test(`throws an error if the env var doesn't exist`, () => {
			process.env.SOME_ENV = undefined;
			const env = new EnvironmentReader();

			expect(() => env.string.get(`SOME_ENV`)).toThrow(/not set/i);
		});
	});

	describe(`env.optional.string.get()`, () => {
		test(`returns a string if the env var exists`, () => {
			process.env.SOME_ENV = `some-string`;
			const env = new EnvironmentReader();

			const result = env.optional.string.get(`SOME_ENV`);

			expect(typeof result).toBe(`string`);
			expect(result).toBe(`some-string`);
		});

		test(`returns undefined if the env var doesn't exist`, () => {
			process.env.SOME_ENV = undefined;
			const env = new EnvironmentReader();

			const result = env.optional.string.get(`SOME_ENV`);

			expect(result).toBe(undefined);
		});
	});

	describe(`env.number.get()`, () => {
		test(`returns a number if the env var exists`, () => {
			process.env.SOME_ENV = `123`;
			const env = new EnvironmentReader();

			const result = env.number.get(`SOME_ENV`);

			expect(typeof result).toBe(`number`);
			expect(result).toBe(123);
		});

		test(`throws an error if the env var doesn't exist`, () => {
			process.env.SOME_ENV = undefined;
			const env = new EnvironmentReader();

			expect(() => env.number.get(`SOME_ENV`)).toThrow(/not set/i);
		});
	});

	describe(`env.optional.number.get()`, () => {
		test(`returns a number if the env var exists`, () => {
			process.env.SOME_ENV = `123`;
			const env = new EnvironmentReader();

			const result = env.optional.number.get(`SOME_ENV`);

			expect(typeof result).toBe(`number`);
			expect(result).toBe(123);
		});

		test(`returns undefined if the env var doesn't exist`, () => {
			process.env.SOME_ENV = undefined;
			const env = new EnvironmentReader();

			const result = env.optional.number.get(`SOME_ENV`);

			expect(result).toBe(undefined);
		});
	});

	describe(`env.boolean.get()`, () => {
		test(`returns a truthy boolean for value "true" if the env var exists`, () => {
			process.env.SOME_ENV = `true`;
			const env = new EnvironmentReader();

			const result = env.boolean.get(`SOME_ENV`);

			expect(typeof result).toBe(`boolean`);
			expect(result).toBe(true);
		});

		test(`returns a truthy boolean for value "1" if the env var exists`, () => {
			process.env.SOME_ENV = `1`;
			const env = new EnvironmentReader();

			const result = env.boolean.get(`SOME_ENV`);

			expect(typeof result).toBe(`boolean`);
			expect(result).toBe(true);
		});

		test(`returns a falsy boolean for value "false" if the env var exists`, () => {
			process.env.SOME_ENV = `false`;
			const env = new EnvironmentReader();

			const result = env.boolean.get(`SOME_ENV`);

			expect(typeof result).toBe(`boolean`);
			expect(result).toBe(false);
		});

		test(`returns a falsy boolean for value "0" if the env var exists`, () => {
			process.env.SOME_ENV = `0`;
			const env = new EnvironmentReader();

			const result = env.boolean.get(`SOME_ENV`);

			expect(typeof result).toBe(`boolean`);
			expect(result).toBe(false);
		});

		test(`returns a falsy boolean for non-matched values if the env var exists`, () => {
			process.env.SOME_ENV = `some-non-matched-value`;
			const env = new EnvironmentReader();

			const result = env.boolean.get(`SOME_ENV`);

			expect(typeof result).toBe(`boolean`);
			expect(result).toBe(false);
		});

		test(`throws an error if the env var doesn't exist`, () => {
			process.env.SOME_ENV = undefined;
			const env = new EnvironmentReader();

			expect(() => env.boolean.get(`SOME_ENV`)).toThrow(/not set/i);
		});
	});

	describe(`env.optional.boolean.get()`, () => {
		test(`returns a truthy boolean for value "true" if the env var exists`, () => {
			process.env.SOME_ENV = `true`;
			const env = new EnvironmentReader();

			const result = env.optional.boolean.get(`SOME_ENV`);

			expect(typeof result).toBe(`boolean`);
			expect(result).toBe(true);
		});

		test(`returns a truthy boolean for value "1" if the env var exists`, () => {
			process.env.SOME_ENV = `1`;
			const env = new EnvironmentReader();

			const result = env.optional.boolean.get(`SOME_ENV`);

			expect(typeof result).toBe(`boolean`);
			expect(result).toBe(true);
		});

		test(`returns a falsy boolean for value "false" if the env var exists`, () => {
			process.env.SOME_ENV = `false`;
			const env = new EnvironmentReader();

			const result = env.optional.boolean.get(`SOME_ENV`);

			expect(typeof result).toBe(`boolean`);
			expect(result).toBe(false);
		});

		test(`returns a falsy boolean for value "0" if the env var exists`, () => {
			process.env.SOME_ENV = `0`;
			const env = new EnvironmentReader();

			const result = env.optional.boolean.get(`SOME_ENV`);

			expect(typeof result).toBe(`boolean`);
			expect(result).toBe(false);
		});

		test(`returns a falsy boolean for non-matched values if the env var exists`, () => {
			process.env.SOME_ENV = `some-non-matched-value`;
			const env = new EnvironmentReader();

			const result = env.optional.boolean.get(`SOME_ENV`);

			expect(typeof result).toBe(`boolean`);
			expect(result).toBe(false);
		});

		test(`returns undefined if the env var doesn't exist`, () => {
			process.env.SOME_ENV = undefined;
			const env = new EnvironmentReader();

			const result = env.optional.boolean.get(`SOME_ENV`);

			expect(result).toBe(undefined);
		});
	});
});
