import type { DataType, ValueType } from './definitions';

/**
 * Represents the possible errors when validating an environment variable key.
 */
export type ErrValidateKey = `ERR_EMPTY_STRING` | `ERR_REGEX_FAIL`;

/**
 * Validates the given key for an environment variable and returns the formatted key.
 * @param key - The input environment variable key.
 */
export function validateKey(key: string): [ErrValidateKey, undefined] | [undefined, string] {
	const output = key.trim();

	switch (true) {
		case !output:
			return [`ERR_EMPTY_STRING`, undefined];

		case !/^[A-Z_][A-Z0-9_]+$/.test(output):
			return [`ERR_REGEX_FAIL`, undefined];

		default:
			return [undefined, output];
	}
}

/**
 * Extracts and returns the given environment variable by key and optionally trimming the value.
 * @param key - The key of the environment variable to extract.
 * @param noTrim - Whether to trim the value.
 */
export function extractValue(key: string, noTrim?: boolean): string | undefined {
	const value = process.env[key];
	return noTrim ? value : value?.trim();
}

/**
 * Parses the given input value into a boolean.
 * @param input - The input value to parse.
 */
export function parseBool(input: string | undefined): boolean {
	if (input && [`true`, `1`].includes(input.toLowerCase())) return true;
	return false;
}

/**
 * Casts the given input value to a string.
 * @param input - The input value to cast.
 */
function castValueToString<EnvType extends ValueType>(input: string | undefined): EnvType {
	if (input === undefined) return undefined as unknown as EnvType;
	return String(input) as unknown as EnvType;
}

/**
 * Casts the given input value to a number.
 * @param input - The input value to cast.
 */
function castValueToNumber<EnvType extends ValueType>(input: string | undefined): EnvType {
	if (input === undefined) return undefined as unknown as EnvType;
	return Number(input) as unknown as EnvType;
}

/**
 * Casts the given input value to a boolean.
 * @param input - The input value to cast.
 */
function castValueToBoolean<EnvType extends ValueType>(input: string | undefined): EnvType {
	if (input === undefined) return undefined as unknown as EnvType;
	return parseBool(input) as unknown as EnvType;
}

/**
 * Returns the given input value into the given data type.
 * @param input - The input value to cast.
 * @param castTo - The type to cast the value to.
 */
export function castValue<EnvType extends ValueType>(input: string | undefined, castTo: DataType): EnvType {
	if (castTo === `NUMBER`) return castValueToNumber<EnvType>(input);
	if (castTo === `BOOLEAN`) return castValueToBoolean<EnvType>(input);
	return castValueToString<EnvType>(input);
}

/**
 * Returns true if the given value is one of the given allowed values. This function should only ever be called after
 * the value has been typecasted.
 * @param value - The typecasted value of the environment variable to check.
 * @param allowedValues - A list of allowed values.
 * @param optional - Whether or not the value is optional.
 */
export function validateValue<EnvType extends ValueType>(
	value: EnvType,
	allowedValues: ValueType[],
	optional: boolean,
): boolean {
	if (!allowedValues.length) return true;
	if (optional && value === undefined) return true;

	const isAllowed = !!allowedValues.find(allowedValue => allowedValue === value);
	return isAllowed;
}
