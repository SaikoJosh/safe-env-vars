/**
 * An enum representing the possible data types we can cast the environment variable to.
 */
export type DataType = `STRING` | `NUMBER` | `BOOLEAN`;

/**
 * Represents the possible types for the value of the environment variable.
 */
export type ValueType = string | number | boolean | undefined;
