import * as utilities from './utilities';

let defaultProcessEnv: NodeJS.ProcessEnv;

beforeEach(() => {
	defaultProcessEnv = { ...process.env };
});

afterEach(() => {
	process.env = { ...defaultProcessEnv };
});

describe(`#validateKey`, () => {
	const inputMapHappy = {
		'SOME_ENV': `SOME_ENV`,
		// eslint-disable-next-line @typescript-eslint/naming-convention -- used for testing.
		'SOME_ENV_WITH_TRAILING_SPACES   ': `SOME_ENV_WITH_TRAILING_SPACES`, // <-- ensure you leave the trailing spaces here!
	};

	const inputMapUnhappy = {
		/* eslint-disable @typescript-eslint/naming-convention -- used for testing. */
		'': `ERR_EMPTY_STRING`,
		'this is not an env key': `ERR_REGEX_FAIL`,
		'123_CANNOT_START_WITH_NUMBERS': `ERR_REGEX_FAIL`,
		/* eslint-enable @typescript-eslint/naming-convention -- used for testing. */
	};

	Object.entries(inputMapHappy).forEach(([input, expected]) => {
		test(`for input key "${input}" returns no error and the prepared key as "${expected}"`, () => {
			const [errId, preparedKey] = utilities.validateKey(input);

			expect(errId).toBe(undefined);
			expect(preparedKey).toBe(expected);
		});
	});

	Object.entries(inputMapUnhappy).forEach(([input, expected]) => {
		test(`for input key "${input}" returns error "${expected}" and undefined for the prepared key`, () => {
			const [errId, preparedKey] = utilities.validateKey(input);

			expect(errId).toBe(expected);
			expect(preparedKey).toBe(undefined);
		});
	});
});

describe(`#extractValue`, () => {
	test(`extracts the environment variable with trimming`, () => {
		const key = `TEST_KEY`;
		const noTrim = false;
		process.env[key] = `some-value-with-trailing-spaces   `; // <-- ensure you leave the trailing spaces here!

		const result = utilities.extractValue(key, noTrim);

		expect(result).toBe(`some-value-with-trailing-spaces`); // <-- ensure you leave the trailing spaces here!
	});

	test(`extracts the environment variable without trimming`, () => {
		const key = `TEST_KEY`;
		const noTrim = true;
		process.env[key] = `some-value-with-trailing-spaces   `; // <-- ensure you leave the trailing spaces here!

		const result = utilities.extractValue(key, noTrim);

		expect(result).toBe(`some-value-with-trailing-spaces   `); // <-- ensure you leave the trailing spaces here!
	});
});

describe(`#parseBool`, () => {
	const inputMap = {
		'true': true,
		'TRUE': true,
		'tRuE': true,
		'1': true, // eslint-disable-line @typescript-eslint/naming-convention -- used for testing.
		'false': false,
		'FALSE': false,
		'fAlSe': false,
		'0': false, // eslint-disable-line @typescript-eslint/naming-convention -- used for testing.
		'some-other-value': false, // eslint-disable-line @typescript-eslint/naming-convention -- used for testing.
	};

	Object.entries(inputMap).forEach(([input, expected]) => {
		test(`parses input "${input}" to boolean: ${expected}`, () => {
			const result = utilities.parseBool(input);

			expect(result).toBe(expected);
		});
	});

	test(`parses undefined to boolean: false`, () => {
		const input = undefined;

		const result = utilities.parseBool(input);

		expect(result).toBe(false);
	});
});

describe(`#castValue`, () => {
	describe(`strings`, () => {
		test(`returns undefined for input which is undefined`, () => {
			const input = undefined;

			const result = utilities.castValue(input, `STRING`);

			expect(result).toBe(undefined);
		});

		test(`casts the given input to a string`, () => {
			const input = `123`;

			const result = utilities.castValue(input, `STRING`);

			expect(typeof result).toBe(`string`);
			expect(result).toBe(`123`);
		});
	});

	describe(`numbers`, () => {
		test(`returns undefined for input which is undefined`, () => {
			const input = undefined;

			const result = utilities.castValue(input, `NUMBER`);

			expect(result).toBe(undefined);
		});

		test(`casts the given input to a number`, () => {
			const input = `123`;

			const result = utilities.castValue(input, `NUMBER`);

			expect(typeof result).toBe(`number`);
			expect(result).toBe(123);
		});
	});

	describe(`booleans`, () => {
		test(`returns undefined for input which is undefined`, () => {
			const input = undefined;

			const result = utilities.castValue(input, `BOOLEAN`);

			expect(result).toBe(undefined);
		});

		test(`casts the given input to a boolean`, () => {
			const input = `true`;

			const result = utilities.castValue(input, `BOOLEAN`);

			expect(typeof result).toBe(`boolean`);
			expect(result).toBe(true);
		});
	});
});
