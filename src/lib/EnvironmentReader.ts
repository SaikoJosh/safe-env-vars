import * as dotenv from 'dotenv';
import { validateKey, extractValue, castValue, validateValue } from './utilities';
import type { DataType, ValueType } from './definitions';

/**
 * Options used by the env reader instance.
 */
interface ClassOptions {
	dotEnv: boolean;
	dotEnvPath: string | undefined;
}

/**
 * Options that can be passed to the constructor of the env reader.
 */
export type ClassOptionsInput = Partial<ClassOptions>;

/**
 * Options that can be passed to the read method.
 */
interface ReadOptions {
	optional: boolean;
	allowEmpty: boolean;
	allowedValues: ValueType[];
	noTrim: boolean;
	castTo: DataType;
}

/**
 * The default options for the read method.
 */
const DEFAULT_READ_OPTIONS: ReadOptions = {
	optional: false,
	allowEmpty: false,
	allowedValues: [],
	noTrim: false,
	castTo: `STRING`,
};

/**
 * Options that can be passed to the get methods.
 */
export interface GetOptionsInput {
	allowEmpty?: boolean;
	allowedValues?: ValueType[];
}

/**
 * Represents a generic 'get' method.
 */
type GetMethod<EnvType> = (key: string, options?: GetOptionsInput) => EnvType;

/**
 * Represents the '.string' object on class instances.
 */
interface StringDict<EnvType extends string | undefined = string> {
	get: GetMethod<EnvType extends undefined ? string | undefined : string>;
}

/**
 * Represents the '.number' object on class instances.
 */
interface NumberDict<EnvType extends number | undefined = number> {
	get: GetMethod<EnvType extends undefined ? number | undefined : number>;
}

/**
 * Represents the '.boolean' object on class instances.
 */
interface BooleanDict<EnvType extends boolean | undefined = boolean> {
	get: GetMethod<EnvType extends undefined ? boolean | undefined : boolean>;
}

/**
 * Represents the '.optional' object on class instances.
 */
interface OptionalDict {
	get: GetMethod<string | undefined>;
	string: StringDict<undefined>;
	number: NumberDict<undefined>;
	boolean: BooleanDict<undefined>;
}

/**
 * Class loads environment variable values from the environment.
 */
export default class EnvironmentReader {
	/**
	 * Class instance options, default values.
	 */
	private readonly options: ClassOptions = { dotEnv: true, dotEnvPath: undefined };

	/**
	 * Reads in the given environment variable as a string.
	 * @param key - The key of the environment variable to read.
	 * @param options - A dictionary of options to customise the behaviour of the method.
	 */
	public readonly get: GetMethod<string> = (key, options) =>
		this.read<string>(key, { ...DEFAULT_READ_OPTIONS, ...options, optional: false });

	/**
	 * Contains methods that will type cast to string.
	 */
	public readonly string: StringDict = {
		/**
		 * Reads in the given environment variable as a string.
		 * @param key - The key of the environment variable to read.
		 * @param options - A dictionary of options to customise the behaviour of the method.
		 */
		get: (key, options) =>
			this.read<string>(key, { ...DEFAULT_READ_OPTIONS, ...options, optional: false, castTo: `STRING` }),
	};

	/**
	 * Contains methods that will type cast to number.
	 */
	public readonly number: NumberDict = {
		/**
		 * Reads in the given environment variable as a number.
		 * @param key - The key of the environment variable to read.
		 * @param options - A dictionary of options to customise the behaviour of the method.
		 */
		get: (key, options) =>
			this.read<number>(key, { ...DEFAULT_READ_OPTIONS, ...options, optional: false, castTo: `NUMBER` }),
	};

	/**
	 * Contains methods that will type cast to boolean.
	 */
	public readonly boolean: BooleanDict = {
		/**
		 * Reads in the given environment variable as a boolean.
		 * @param key - The key of the environment variable to read.
		 * @param options - A dictionary of options to customise the behaviour of the method.
		 */
		get: (key, options) =>
			this.read<boolean>(key, { ...DEFAULT_READ_OPTIONS, ...options, optional: false, castTo: `BOOLEAN` }),
	};

	/**
	 * Contains methods that allow optional environment variables.
	 */
	public readonly optional: OptionalDict = {
		/**
		 * Reads in the given environment variable as an optional string.
		 * @param key - The key of the environment variable to read.
		 * @param options - A dictionary of options to customise the behaviour of the method.
		 */
		get: (key, options) => this.read<string>(key, { ...DEFAULT_READ_OPTIONS, ...options, optional: true }),

		/**
		 * Contains methods that will type cast to string.
		 */
		string: {
			/**
			 * Reads in the given environment variable as an optional string.
			 * @param key - The key of the environment variable to read.
			 * @param options - A dictionary of options to customise the behaviour of the method.
			 */
			get: (key, options) =>
				this.read<string>(key, { ...DEFAULT_READ_OPTIONS, ...options, optional: true, castTo: `STRING` }),
		},

		/**
		 * Contains methods that will type cast to number.
		 */
		number: {
			/**
			 * Reads in the given environment variable as an optional number.
			 * @param key - The key of the environment variable to read.
			 * @param options - A dictionary of options to customise the behaviour of the method.
			 */
			get: (key, options) =>
				this.read<number>(key, { ...DEFAULT_READ_OPTIONS, ...options, optional: true, castTo: `NUMBER` }),
		},

		/**
		 * Contains methods that will type cast to boolean.
		 */
		boolean: {
			/**
			 * Reads in the given environment variable as an optional boolean.
			 * @param key - The key of the environment variable to read.
			 * @param options - A dictionary of options to customise the behaviour of the method.
			 */
			get: (key, options) =>
				this.read<boolean>(key, { ...DEFAULT_READ_OPTIONS, ...options, optional: true, castTo: `BOOLEAN` }),
		},
	};

	/**
	 * @param options - The class options to set, optional.
	 */
	public constructor(options: ClassOptionsInput = {}) {
		this.options = { ...this.options, ...options };
		if (this.options.dotEnv) dotenv.config({ path: this.options.dotEnvPath });
	}

	/**
	 * Reads in the given environment variable, handling any configured options, and data type casting.
	 * @param key - The key of the environment variable to read.
	 * @param options - A dictionary of options to customise the behaviour of the method.
	 */
	private read<EnvType extends ValueType>(key: string, options: ReadOptions): EnvType {
		const { optional, allowEmpty, allowedValues, noTrim, castTo } = options;

		const [errId, preparedKey] = validateKey(key);
		if (errId) throw new Error(`[${errId}] The env key "${key}" is not valid`);

		const value = extractValue(preparedKey, noTrim);
		if (!optional && value === undefined) {
			throw new Error(`Env variable "${preparedKey}" is required but is not set`);
		}
		if (!allowEmpty && value === ``) {
			throw new Error(`Env variable "${preparedKey}" is required but is empty`);
		}

		const typedValue = castValue<EnvType>(value, castTo);
		if (!validateValue<EnvType>(typedValue, allowedValues, optional)) {
			throw new Error(`Env variable "${preparedKey}" is "${value}" but must be one of: "${allowedValues.join(`,`)}"`);
		}

		return typedValue;
	}
}
