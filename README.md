# Safe Environment Variables

Read environment variables in a safe way, with full TypeScript support. No more missing environment variables!

# Quick Start

By default, `safe-env-vars` will read environment variables from the environment as well as a `.env` file. It will also throw an error by default if the environment variable you're trying to read is undefined. See the FAQ at the bottom for some helpful hints.

```ts
import { EnvironmentReader } from 'safe-env-vars';

const env = new EnvironmentReader();

const myVar = env.get(`SOME_VARIABLE`); // string
const port = env.number.get(`PORT`); // number
const optionalFlag = env.optional.boolean.get(`COOL_FEATURE_FLAG`); // boolean | undefined
```

# Constructor Options

The following options can be passed to the constructor when instantiating the class.

| Option       | Default                     | Required | Description                                      |
| ------------ | --------------------------- | -------- | ------------------------------------------------ |
| `dotEnv`     | `true`                      | -        | Set to `false` to disable dot env functionality. |
| `dotEnvPath` | _Current working directory_ | -        | Specify the path to the dot env file, optional.  |

# Methods

An EnvironmentReader instance has the following methods. Each method takes a `key` string and an optional `options` dictionary. See the section on 'get method options' for the shape of `options`.

### **➡️ `env.get(key, options?)`**

Reads the environment variable at `key` as a `string` and throws an error if it doesn't exist.

### **➡️ `env.string.get(key, options?)`**

Same as `env.get(key, options?)`.

### **➡️ `env.number.get(key, options?)`**

Reads the environment variable at `key` casting to a `number` and throws an error if it doesn't exist.

### **➡️ `env.boolean.get(key, options?)`**

Reads the environment variable at `key` casting to a `boolean` and throws an error if it doesn't exist.

### **➡️ `env.optional.get(key, options?)`**

Reads the environment variable at `key` as a `string`. Returns `undefined` if the variable doesn't exist.

### **➡️ `env.optional.string.get(key, options?)`**

Same as `env.optional.get(key, options?)`.

### **➡️ `env.optional.number.get(key, options?)`**

Reads the environment variable at `key` casting to a `number`. Returns `undefined` if the variable doesn't exist.

### **➡️ `env.optional.boolean.get(key, options?)`**

Reads the environment variable at `key` casting to a `boolean`. Returns `undefined` if the variable doesn't exist.

# Get Method Options

The following options can be passed to any `.get()` method.

| Option          | Type    | Default | Required | Description                                                            |
| --------------- | ------- | ------- | -------- | ---------------------------------------------------------------------- |
| `allowEmpty`    | Boolean | `false` | -        | Set to `true` if you want to allow the variable to be an empty string. |
| `allowedValues` | any[]   |         | -        | Pass an array of allowed values to verify against the typecast value.  |

# FAQ

## What is `safe-env-vars`?

`Safe-env-vars` provides a way to read environment variables in a safe way, with full TypeScript support and typecasting to the correct data type. By default, an error is thrown if a variable is missing.

## How do I load environment variables from a .env file?

`Safe-env-vars` automatically reads environment variables from a .env file. To customise the path to the .env file specify the `dotEnvPath` option when constructing `EnvironmentReader`.

## How do I disable .env functionality?

If you don't want `safe-env-vars` to read from a .env file by default you can disable it by passing `false` to the `dotEnv` option when constructing `EnvironmentReader`.

## Do I need to install dotenv in my own project?

Nope. `Safe-env-vars` uses dotenv internally.

## How do I allow optional variables?

By default, `safe-env-vars` will throw an error if an environment variable is undefined. To allow undefined variables, use any of the `.get()` methods that include `.optional` in the chain, e.g. `env.optional.get('SOME_VARIABLE')` or `env.optional.number.get('SOME_VARIABLE')`. See the section on 'methods' for the full list.

## How do I allow an empty string as a variable?

By default, `safe-env-vars` will throw an error if a variable exists but it's an empty string. You can change this by passing `allowEmpty: true` to the `options` parameter of any `.get()` method.

## How do I cast variables to a number/boolean type?

Use any of the `.get()` methods that include a type modifier, e.g. `env.number.get('SOME_VARIABLE')` or `env.boolean.get('SOME_VARIABLE')`. See the section on 'methods' for the full list.

## How do I verify the value of the variable is what I expect?

You can pass a list of allowed values to any `.get()` method. If the value of the environment variable is not one of the allowed values at runtime, an error will be thrown.

```ts
const value = env.string.get(`SOME_STR`, { allowedValues: [`ABC`, `DEF`] }); // string, must be "ABC" or "DEF".
const value = env.number.get(`SOME_NUM`, { allowedValues: [461, 582, 923] }); // number, must be 461, 582, or 923.
const value = env.boolean.get(`SOME_BOOL`, { allowedValues: [true] }); // boolean, can only be true.
```
