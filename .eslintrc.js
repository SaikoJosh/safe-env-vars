const { eslint } = require("./node_modules/quickstartconfig");

module.exports = {
  ...eslint,
};
