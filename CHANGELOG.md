# Changelog

### **`v1.0.5`** / 2022-12-09

- `Fix:` export class as `EnvironmentReader` and `EnvironmentLoader`.

### **`v1.0.4`** / 2022-05-08

- `New:` add `allowedValues` option for verifying if the actual value of the environment variable is allowed.
- `New:` export useful utility types.

### **`v1.0.3`** / 2022-05-04

- `Docs:` update package name & small tweaks.

### **`v1.0.2`** / 2022-05-02

- `Docs:` update package name & small tweaks.

### **`v1.0.1`** / 2022-05-02

- `Docs:` update package name & small tweaks.

### **`v1.0.0`** / 2022-05-02

- `New:` initial release.
