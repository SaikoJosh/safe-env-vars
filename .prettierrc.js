const { prettier } = require('./node_modules/quickstartconfig');

module.exports = {
	...prettier,
};
